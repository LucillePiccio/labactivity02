package hellopackage;
import java.util.Scanner;
import java.util.Random;
//importing a package that is not from a java utils package
import secondpackage.Utilities;

public class Greeter{
    public static void main(String[] args){
        //Creating Scanner object
        Scanner scanner = new Scanner(System.in);

        //Importing and creating Random object
        //java.util.Random rand = new java.util.Random();
        Random rand = new Random();

        System.out.println("Hi, what's your name?");
        String name = scanner.next();

        int randNum = rand.nextInt(11);
        System.out.println("The random number is " + randNum);

        //Asking the user to input an integer value
        System.out.println("Please enter a number");
        int input = scanner.nextInt();

        //trying doubleMe method from utilities class
        Utilities util = new secondpackage.Utilities();
        util.doubleMe(input);
        System.out.println("The user inputted the number " + input);
    }
}